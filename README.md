# Fetch from springer.com

springer.com made some e-books freely downloadable from their website since the COVID lockdown.

A list of free ebooks is available there:
https://docs.google.com/spreadsheets/d/1HzdumNltTj2SHmCv3SRdoub8SvpIEn75fa4Q23x0keU/htmlview

# How it works ?

Download the spreadsheet as a CSV (comma separated) file in the same directory than the script.
It'll browse the file and download the book, put the right file name and organize the books by category.

The CSV file format should be like this:

> Author, English Package Name, OpenURL
