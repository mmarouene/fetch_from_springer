#!/usr/bin/env python
from bs4 import BeautifulSoup, SoupStrainer
import requests
import csv
import os

def f_get_pdf_link():
	skip_create = 0
	data = page.text
	soup = BeautifulSoup(data, "lxml")
	link = soup.find("a", title="Download this book in PDF format")
	try:
		bookpath = link.get('href')
		bookurl = ('https://link.springer.com' + bookpath)
	except:
		print (filename +" - not freely downloadable")
		bookurl = 0
		skip_create = 2
	return bookurl,skip_create;

def f_check_file():
	skip_get = 0
	if not os.path.exists(category):
		os.makedirs(category)
	if os.path.isfile(filename):
		print (filename +" - already exists")
		skip_get = 1
	return (skip_get)

def f_create_file():
	print (filename +" - downloading")
	ebook = requests.get(bookurl)
	open(filename, 'wb').write(ebook.content)

#with open('test.csv', mode='r') as csv_file:
with open('Free_Springer_Books-eBook_list.csv', mode='r') as csv_file:
	csv_reader = csv.DictReader(csv_file)
	line_count = 2
	for row in csv_reader:
		skip_get = 0
		skip_create = 0
		booktitle = row["Book Title"]
		category = row["English Package Name"]
		page = requests.get(row["OpenURL"])
		filename = os.path.join(str(category), str(booktitle) +".pdf")
		skip_get = f_check_file()
		if skip_get != 1:
			bookurl, skip_create = f_get_pdf_link()
			if skip_create != 2:
				f_create_file()
		else:
			print ("download skipped")
		print(f'processed line {line_count}. '+ booktitle +"\n")
		line_count += 1
# vim: ts=4 sw=4 sts=4 sr noet ai si
